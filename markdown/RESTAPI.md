# RESTFul API 设计规范

> 统一规范API设计模式，降低不同系统和设备间的沟通的成本。

## 通用说明

> 接口统一采用JSON作为数据交换格式

标准响应报文

```json
{
    "code": 0,
    "message": "ok",
    "data": {
        "key1": "value1",
        "key2": "value2"
    },
    "result": {},
    "timestamp": 1522157490000
}
```

 字段 | 名字 | 类型 | 说明 
-----|------|------|-----
code | 状态码 | int | 0: 成功, 1:失败 -1:异常 其他自定义
message| 消息| string| 消息内容
data| 数据 | object | 键值对对象， key -> value
result| 结果对象 | object or array | 响应结果，数组或对象
timestamp| 时间戳 | long | Unix时间戳

**PS.** 其中`code`状态码和`timestamp`必须存在，其他视情况而定

精简响应报文,对应以上字段

`c` -> `code`, `m` -> `message`, `d` -> `data`, `r` -> `result` , `t` -> `timestamp`

```json
{
    "c": 0,
    "m": "ok",
    "d": {"k":"v"},
    "r": [],
    "t": 152215749000
}
```

标准响应报文2

```json
{
    "status": "1",
    "message": "ok",
    "result": {}
}
```

 字段 | 名字 | 类型 | 说明 
-----|------|------|-----
status | 状态码 | string | "1": 成功，其他错误码自定
message| 消息| string| 消息内容
result| 结果对象 | object or array | 响应结果，数组或对象


## 接口定义

**域名**

尽量将API部署在专用域名之下。

```text
https://api.example.com
```

如果API简单，可考虑在主域名之下

```text
https://example.com/api
```

**版本号**

版本号放入URL。

```text
https://api.example.com/v1
```

**设计用列**

> 在RESTful架构中，每个网址代表一种资源（resource），所以网址中不能有动词，只能有名词，而且所用的名词往往与数据库的表格名对应。一般来说，数据库中的表都是同种记录的"集合"（collection），所以API中的名词也应该使用复数。

```text
拉取资源列表 `GET /v1/contents?offset=0&limit=15`
新建一个资源 `POST /v1/contents`
更新资源信息 `PUT /v1/contents`
获取一个资源 `GET /v1/contents/{ID}`
删除资源信息 `DELETE /v1/contents/{ID}`
```