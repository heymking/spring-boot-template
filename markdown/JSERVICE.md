# JAVA服务接口命名和设计规范

> 良好的接口命名可以提高代码的可读性以及维护性，也是代码整洁重构的第一步。

## 类JPA风格命名

> Java Service 接口推荐使用类JPA接口风格命名

**语义明确**

```text
Object findOne(String id);
Object findById(String id);
Object findByMacAddress(String address);
int updateAndEvictCache(Object obj);
```

部分参考

方法名 | 关键字 | sql
------|-------|------
findById|		|where id=?
findByIdIs|	is	|where id=?
findByIdEquals	|equals	|where id=?
findByNameAndAge	|and	|where name=? and age=?
findByNameOrAge	|or	|where name=? or age=?
findByNameOrderByAgeDesc|	order by|	where name=? order by age desc
findByAgeNotIn	|not in	|where age not in(?)
findByStatusTrue	|true	|where status=true
findByStatusFalse	|false	|where status=false
findByAgeBetween	|between	|where age between ? and ?
findByNameNot	|not	|where name <> ?
findByAgeLessThan	|LessThan	|where age< ?
findByAgeLessThanEqual	|LessThanEqual	|where age<=?
findByAgeGreaterThan	|GreaterThan	|where age>?
findByAgeGreaterThanEqual	|GreaterThanEqual	|where age>=?
findByAgeAfter	|after	|where age>?
findByAgeBefore	|before	|where age< ?
findByNameIsNull	|is null	|where name is null
findByNameNotNull	|not null	|where name not null
findByNameLike	|like	|where name like ?
findByNameNotLike	|not like	w|here name not like ?
findByNameStartingWith	|StartingWith	|where name like ‘?%’
findByNameEndingWith	|EndingWith	|where name like ‘%?’
findByNameContaining	|Containing	|where name like ‘%?%’