package com.brtbeacon.example.data.service;

import com.brtbeacon.common.service.BaseService;
import com.brtbeacon.example.data.entity.Content;
import com.brtbeacon.example.data.entity.ContentExample;

/**
 * ContentService
 *
 * @author Archx[archx@foxmail.com]
 * @date 2018/03/27 1149
 */
public interface ContentService extends BaseService<Content, ContentExample, Integer> {

    int saveOrUpdate(Content content);
}