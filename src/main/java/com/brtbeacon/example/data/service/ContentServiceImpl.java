package com.brtbeacon.example.data.service;

import com.brtbeacon.common.service.BaseServiceSupport;
import com.brtbeacon.example.data.entity.Content;
import com.brtbeacon.example.data.entity.ContentExample;
import com.brtbeacon.example.data.repository.ContentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ContentServiceImpl
 *
 * @author Archx[archx@foxmail.com]
 * @date 2018/03/27 1149
 */
@Service
public class ContentServiceImpl extends BaseServiceSupport<Content, ContentExample, Integer> implements ContentService {
    private ContentMapper mapper;

    @Autowired
    public void setMapper(ContentMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public ContentMapper getMapper() {
        return this.mapper;
    }

    @Override
    public int saveOrUpdate(Content content) {
        return content.getId() != null ? update(content) : save(content);
    }
}