package com.brtbeacon.example.data.entity;

import com.brtbeacon.common.entity.BaseEntity;

/**
 * Content
 * 
 * @author Archx[archx@foxmail.com]
 * @date 2018/03/27 1149
 */
public class Content extends BaseEntity<Integer> {
    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}