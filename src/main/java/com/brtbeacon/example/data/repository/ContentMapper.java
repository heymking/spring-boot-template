package com.brtbeacon.example.data.repository;

import com.brtbeacon.example.data.entity.Content;
import com.brtbeacon.example.data.entity.ContentExample;
import com.brtbeacon.persistent.mybatis.BaseExampleMapper;
import com.brtbeacon.persistent.mybatis.annotation.MyBatisRepository;

/**
 * ContentMapper
 * 
 * @author Archx[archx@foxmail.com]
 * @date 2018/03/27 1149
 */
@MyBatisRepository
public interface ContentMapper extends BaseExampleMapper<Content, ContentExample, Integer> {
}