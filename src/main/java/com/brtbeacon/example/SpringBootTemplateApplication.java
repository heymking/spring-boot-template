package com.brtbeacon.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBootTemplateApplication
 *
 * @author Archx[archx@foxmail.com]
 * @date 2018/3/27 10:27
 */
@SpringBootApplication
public class SpringBootTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTemplateApplication.class, args);
    }
}
