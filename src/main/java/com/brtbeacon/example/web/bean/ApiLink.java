package com.brtbeacon.example.web.bean;

/**
 * ApiLink
 *
 * @author Archx[archx@foxmail.com]
 * @date 2018/3/27 11:59
 */
public class ApiLink {

    private String title;

    private String href;

    private String description;

    private String method;

    public ApiLink(String title, String href, String description, String method) {
        this.title = title;
        this.href = href;
        this.description = description;
        this.method = method;
    }

    public String getTitle() {
        return title;
    }

    public String getHref() {
        return href;
    }

    public String getDescription() {
        return description;
    }

    public String getMethod() {
        return method;
    }
}
