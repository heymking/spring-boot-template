package com.brtbeacon.example.web.controller;

import com.brtbeacon.commvc.entity.RespResult;
import com.brtbeacon.example.data.entity.Content;
import com.brtbeacon.example.data.entity.ContentExample;
import com.brtbeacon.example.data.service.ContentService;
import com.brtbeacon.example.web.bean.ApiLink;
import com.brtbeacon.persistent.mybatis.entity.PageBounds;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * 数据接口控制器
 * <p>
 * 使用 @RestController 注解，方法返回默认加上 @ResponseBody<br/>
 * 方法响应体必须为RespResult
 *
 * @author Archx[archx@foxmail.com]
 * @date 2018/3/27 10:37
 */
@RestController
@RequestMapping("/data")
public class DataController {

    @Resource
    private ContentService contentService;

    /**
     * Index Endpoint
     * <p>
     * 如果提供了基础路径，则返回当前接口下提供的方法及其描述
     *
     * @return json
     */
    @RequestMapping
    public RespResult index() {
        RespResult.Builder builder = new RespResult.Builder();

        ApiLink link1 = new ApiLink("get /data/contents", "/data/contents", "获取所有资源列表", "GET");
        ApiLink link2 = new ApiLink("post /data/contents", "/data/contents", "存储或更新", "POST");
        ApiLink link3 = new ApiLink("get /data/contents/{id}", "/data/contents/{id}", "返回目标资源", "GET");
        ApiLink link4 = new ApiLink("delete /data/contents/{id}", "//data/contents/{id}", "删除目标资源", "DELETE");

        List<ApiLink> apiLinks = Arrays.asList(link1, link2, link3, link4);
        builder.message("data api list").result(apiLinks);
        return builder.build();
    }

    /**
     * 返回所有资源
     *
     * @return JSON
     */
    @GetMapping("/contents")
    public RespResult contents(@RequestParam(value = "search", required = false) String search,
                               @RequestParam("offset") Integer offset, @RequestParam("limit") Integer limit) {

        ContentExample example = new ContentExample();
        if (StringUtils.hasText(search)) {
            example.createCriteria().andTitleLike('%' + search + '%');
            example.or().andContentLike('%' + search + '%');
            example.setOrderByClause("created_time desc");
        }
        PageBounds page = new PageBounds(offset, limit);
        List<Content> contents = contentService.findByExample(example, page);
        return new RespResult.Builder().data("total", page.getTotal()).result(contents).build();
    }

    /**
     * 存储或更新
     *
     * @param content content
     * @return json
     */
    @PostMapping("/contents")
    public RespResult contents(Content content) {
        int saveOrUpdate = contentService.saveOrUpdate(content);
        return new RespResult.Builder().build(saveOrUpdate > 0);
    }

    /**
     * 返回目标资源
     *
     * @param id ID
     * @return json
     */
    @GetMapping("/contents/{id}")
    public RespResult getContent(@PathVariable("id") Integer id) {
        return new RespResult.Builder().result(contentService.findOne(id)).build();
    }

    /**
     * 删除目标资源
     *
     * @param id ID
     * @return json
     */
    @DeleteMapping("/contents/{id}")
    public RespResult delContent(@PathVariable("id") Integer id) {
        int delete = contentService.delete(id);
        return new RespResult.Builder().build(delete > 0);
    }
}
