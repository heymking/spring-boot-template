package com.brtbeacon.example.web.controller;

import com.brtbeacon.commvc.entity.RespResult;
import com.brtbeacon.commvc.web.util.RequestUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * IndexController
 *
 * @author Archx[archx@foxmail.com]
 * @date 2018/3/27 10:31
 */
@Controller
@RequestMapping("/")
public class IndexController {

    /**
     * 返回首页视图
     * <p>
     * 所有常规视图返回必须使用GET请求
     *
     * @return view
     */
    @GetMapping // or @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    /**
     * JSON 响应
     * <p>
     * 所有接口响应类必须指定为 RespResult
     *
     * @param request request
     * @return json
     */
    @PostMapping
    @ResponseBody
    public RespResult index(HttpServletRequest request) {
        String ipAddress = RequestUtils.getIpAddress(request);
        return new RespResult.Builder().result(ipAddress).build();
    }
}
