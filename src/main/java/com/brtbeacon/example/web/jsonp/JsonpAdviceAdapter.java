package com.brtbeacon.example.web.jsonp;

import com.brtbeacon.example.web.controller.DataController;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

/**
 * JsonpAdviceAdapter
 *
 * @author Archx[archx@foxmail.com]
 * @date 2018/3/27 12:23
 */
@ControllerAdvice(assignableTypes = DataController.class)
public class JsonpAdviceAdapter extends AbstractJsonpResponseBodyAdvice {

    public JsonpAdviceAdapter() {
        super("callback", "jsonp");
    }
}
