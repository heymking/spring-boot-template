package com.brtbeacon.example.configuration;

import com.brtbeacon.commvc.web.controller.DefaultErrorController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * BizConfiguration
 *
 * @author Archx[archx@foxmail.com]
 * @date 2018/3/27 12:18
 */
@Configuration
public class BizConfiguration {

    @Bean // 错误控制器
    public DefaultErrorController defaultErrorController() {
        return new DefaultErrorController();
    }
}
