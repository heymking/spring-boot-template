/**
 * package-info
 * <p>
 * 所有系统配置项必须位于改包下
 *
 * @author Archx[archx@foxmail.com]
 * @date 2018/3/27 10:30
 */
package com.brtbeacon.example.configuration;