#!/bin/bash
# 程序启动脚本模板
# usage: su - root -c bootstrap.sh start

# 进入脚本目录
cd `dirname $0`

LOG=echo

PRONAME="spring-boot-template.jar"

case $1 in
    start)
    ps -ef | grep $PRONAME | grep -v grep >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        $LOG "[INFO] The program $PRONAME is running."
        exit
    else
        $LOG "[INFO] Start program $PRONAME ..."
        nohup java -server -Xms3200m -Xmx3200m -XX:-UseGCOverheadLimit -XX:NewRatio=1 -XX:SurvivorRatio=8 -XX:+UseSerialGC -Dloader.path=. -Djava.security.egd=file:/dev/./urandom -jar $PRONAME  >/dev/null 2>&1 &
        PID=`ps -ef|grep -i $PRONAME|grep -v "grep"|awk '{print $2}'`
        $LOG "[INFO] Start completed. PID:$PID"
    fi
    ;;
    stop)
    ps -ef | grep $PRONAME | grep -v grep >/dev/null 2>&1
    if [ $? -eq 1 ]; then
        $LOG "[INFO] The program $PRONAME is not running."
        exit
    else
        $LOG "[INFO] Stop the program ..."
        PID=`ps -ef|grep -i $PRONAME|grep -v "grep"|awk '{print $2}'`
        for id in ${PID[*]}
        do
            kill ${id}
            if [ $? -eq 0 ]; then
                $LOG "[INFO] The program PID:$PID is killed"
            else
                $LOG "[ERROR] Kill program PID:$PID failed"
            fi
        done
    fi
    ;;
    *)
    $LOG "[ERROR] Please use start or stop as first arugment"
    ;;
esac
