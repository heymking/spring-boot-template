# Spring-boot-template

> SpringBoot模板项目，暨项目标准结构和基本设计规范最佳实践

## 更新说明

 - 2018-06-19 lib分离；避免全量更新，将`config`与`lib`分离出`jar`包

## 项目结构

项目结构参照以下，常规情况下必须遵守该规范。

```text
|-spring-boot-template
    |-markdown 项目文档
    |-src 资源目录
        |-main 主程序目录
            |-bin 可执行脚本
                bootstrap.sh Linux 启动脚本
            |-conf 程序配置文件
                application.properties 应用配置文件
                application-dev.properties 开发环境配置文件
                application-prod.properties 生产环境配置文件
            |-java Java代码
                |-com.brtbeacon.example 主包名
                    |-configuration 配置类名
                    |-data 数据操作类包名
                        |-entity 实体包名
                        |-repository 持久化包名
                        |-service 服务接口包名
                    |-web 应用类包名
                        |-controller 控制器包名
                    SpringBootTemplateApplicaiton.java 主程序 
            |-resources 资源文件
                |-static 静态资源文件 css,js...
                |-templates 模板文件 html
                mybatis-generator.properties MBG 代码生成器配置文件
                mybatis-generator.xml  MBG 代码生成器配置文件
        |-test  测试用例
            |-java
                |-com.brtbeacon.example.test 测试用例包名
        package.xml 打包配置文件
    .gitignore 版本控制忽略配置文件
    pom.xml MAVEN配置文件
    README.md 项目说明文件
```

**PS.** 其中`example`为项目具体项目包名，根据项目自定义

## 配置及其应用

Maven **POM**文件说明

在`pom.xml`文件中，对于依赖`<dependencies>`可以自行调整和更改，`<build>`,`<profiles>`,`<repositories>`在修改前请明确知晓操作可能造成的影响。

基本依赖，该依赖提供的常规的基础扩展，包括数据源和MyBatis基础扩展，具体可查看`https://gitee.com/heymking/brtbeacon-spring-boot-support`

```xml
<!-- Support -->
<dependency>
    <groupId>com.brtbeacon.spring.boot</groupId>
    <artifactId>brtbeacon-spring-boot-support</artifactId>
    <version>1.2.1</version>
</dependency>
```

**Mybatis Generator**扩展及其应用

配置文件说明

    mybatis-generator.properties 数据库连接及生产类存放位置指定
    mybatis-generator.xml 代码生成器配置文件

具体配置请查看文件

Maven 配置

```xml
<profiles>
    <profile>
        <id>mybatis-generator</id>
        <build>
            <plugins>
                <!-- Mybatis Generator 插件 -->
                <plugin>
                    <!-- 执行 mvn mybatis-generator:generate -e -->
                    <groupId>org.mybatis.generator</groupId>
                    <artifactId>mybatis-generator-maven-plugin</artifactId>
                    <version>1.3.5</version>
                    <executions>
                        <execution>
                            <id>Generate MyBatis Artifacts</id>
                            <goals>
                                <goal>generate</goal>
                            </goals>
                        </execution>
                    </executions>
                    <!-- 依赖的包 -->
                    <dependencies>
                        <dependency>
                            <groupId>com.brtbeacon.mbg</groupId>
                            <artifactId>mybatis-generator-support</artifactId>
                            <version>2.0</version>
                        </dependency>
                        <dependency>
                            <groupId>mysql</groupId>
                            <artifactId>mysql-connector-java</artifactId>
                            <version>5.1.41</version>
                        </dependency>
                    </dependencies>
                    <configuration>
                        <verbose>true</verbose>
                        <overwrite>true</overwrite>
                        <!-- 不指定配置文件路径时默认src/main/resource/generatorConfig.xml -->
                        <configurationFile>src/main/resources/mybatis-generator.xml</configurationFile>
                    </configuration>
                </plugin>
            </plugins>
        </build>
    </profile>
</profiles>
```

执行方式 **IDEA** 

`Maven Projects` -> `Profiles` 勾选 `mybatis-generator`

`Maven Projects` -> 项目模块 -> `Plugins` -> `mybatis-generator`

```text
...
[INFO] --- mybatis-generator-maven-plugin:1.3.5:generate (default-cli) @ spring-boot-template ---
[INFO] Introspecting table tb_content
[INFO] Generating Example class for table tb_content
[INFO] Generating Record class for table tb_content
[INFO] Generating Mapper Interface for table tb_content
[INFO] Generating SQL Map for table tb_content
[INFO] Saving file ContentMapper.xml
[INFO] Saving file ContentExample.java
[INFO] Saving file Content.java
[INFO] Saving file ContentMapper.java
[INFO] Saving file ContentService.java
[INFO] Saving file ContentServiceImpl.java
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 1.932 s
[INFO] Finished at: 2018-03-27T11:49:53+08:00
[INFO] Final Memory: 15M/209M
[INFO] ------------------------------------------------------------------------
```

## lib分离后启动方式

![](assets/lib-struct.png)

启动命令：

> 进入解压目录执行

```shell
java -Dloader.path=. -jar .\spring-boot-template.jar
```

效果（Win10 PowerShell)：

```text
PS E:\Repository\spring-boot-template\target\spring-boot-template-package\spring-boot-template> java "-Dloader.path=." -jar .\spring-boot-template.jar

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v1.5.3.RELEASE)

2018-06-19 11:00:24.636  INFO 2708 --- [           main] c.b.e.SpringBootTemplateApplication      : Starting SpringBootTemplateApplication v1.0 on Pikachu with PID 2708 (E:\Repository\spring-boot-template\target\spring-boot-template-package\spring-boot-template\spring-boot-template.jar started by Pikachu in E:\Repository\spring-boot-template\target\spring-boot-template-package\spring-boot-template)
2018-06-19 11:00:24.639  INFO 2708 --- [           main] c.b.e.SpringBootTemplateApplication      : The following profiles are active: dev
2018-06-19 11:00:24.867  INFO 2708 --- [           main] ationConfigEmbeddedWebApplicationContext : Refreshing org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@e2144e4: startup date [Tue Jun 19 11:00:24 CST 2018]; root of context hierarchy
2018-06-19 11:00:26.273  INFO 2708 --- [           main] org.xnio                                 : XNIO version 3.3.6.Final
2018-06-19 11:00:26.292  INFO 2708 --- [           main] org.xnio.nio                             : XNIO NIO Implementation Version 3.3.6.Final
2018-06-19 11:00:26.363  WARN 2708 --- [           main] io.undertow.websockets.jsr               : UT026009: XNIO worker was not set on WebSocketDeploymentInfo, the default worker will be used
2018-06-19 11:00:26.363  WARN 2708 --- [           main] io.undertow.websockets.jsr               : UT026010: Buffer pool was not set on WebSocketDeploymentInfo, the default pool will be used
2018-06-19 11:00:26.392  INFO 2708 --- [           main] io.undertow.servlet                      : Initializing Spring embedded WebApplicationContext
2018-06-19 11:00:26.392  INFO 2708 --- [           main] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 1527 ms
2018-06-19 11:00:26.511  INFO 2708 --- [           main] o.s.b.w.servlet.ServletRegistrationBean  : Mapping servlet: 'dispatcherServlet' to [/]
2018-06-19 11:00:26.519  INFO 2708 --- [           main] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'characterEncodingFilter' to: [/*]
2018-06-19 11:00:26.520  INFO 2708 --- [           main] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'hiddenHttpMethodFilter' to: [/*]
2018-06-19 11:00:26.521  INFO 2708 --- [           main] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'httpPutFormContentFilter' to: [/*]
2018-06-19 11:00:26.522  INFO 2708 --- [           main] o.s.b.w.servlet.FilterRegistrationBean   : Mapping filter: 'requestContextFilter' to: [/*]
Tue Jun 19 11:00:26 CST 2018 WARN: Establishing SSL connection without server's identity verification is not recommended. According to MySQL 5.5.45+, 5.6.26+ and 5.7.6+ requirements SSL connection must be established by default if explicit option isn't set. For compliance with existing applications not using SSL the verifyServerCertificate property is set to 'false'. You need either to explicitly disable SSL by setting useSSL=false, or set useSSL=true and provide truststore for server certificate verification.
2018-06-19 11:00:27.332  INFO 2708 --- [           main] com.alibaba.druid.pool.DruidDataSource   : {dataSource-1} inited
2018-06-19 11:00:27.923  INFO 2708 --- [           main] s.w.s.m.m.a.RequestMappingHandlerAdapter : Looking for @ControllerAdvice: org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext@e2144e4: startup date [Tue Jun 19 11:00:24 CST 2018]; root of context hierarchy
2018-06-19 11:00:27.934  INFO 2708 --- [           main] s.w.s.m.m.a.RequestMappingHandlerAdapter : Detected ResponseBodyAdvice bean in jsonpAdviceAdapter
2018-06-19 11:00:28.019  INFO 2708 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/data/contents],methods=[GET]}" onto public com.brtbeacon.commvc.entity.RespResult com.brtbeacon.example.web.controller.DataController.contents(java.lang.String,java.lang.Integer,java.lang.Integer)
2018-06-19 11:00:28.021  INFO 2708 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/data/contents],methods=[POST]}" onto public com.brtbeacon.commvc.entity.RespResult com.brtbeacon.example.web.controller.DataController.contents(com.brtbeacon.example.data.entity.Content)
2018-06-19 11:00:28.022  INFO 2708 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/data]}" onto public com.brtbeacon.commvc.entity.RespResult com.brtbeacon.example.web.controller.DataController.index()
2018-06-19 11:00:28.024  INFO 2708 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/data/contents/{id}],methods=[GET]}" onto public com.brtbeacon.commvc.entity.RespResult com.brtbeacon.example.web.controller.DataController.getContent(java.lang.Integer)
2018-06-19 11:00:28.025  INFO 2708 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/data/contents/{id}],methods=[DELETE]}" onto public com.brtbeacon.commvc.entity.RespResult com.brtbeacon.example.web.controller.DataController.delContent(java.lang.Integer)
2018-06-19 11:00:28.026  INFO 2708 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/],methods=[GET]}" onto public java.lang.String com.brtbeacon.example.web.controller.IndexController.index()
2018-06-19 11:00:28.027  INFO 2708 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/],methods=[POST]}" onto public com.brtbeacon.commvc.entity.RespResult com.brtbeacon.example.web.controller.IndexController.index(javax.servlet.http.HttpServletRequest)
2018-06-19 11:00:28.028  INFO 2708 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/error]}" onto public org.springframework.http.ResponseEntity<java.util.Map<java.lang.String, java.lang.Object>> com.brtbeacon.commvc.web.controller.DefaultErrorController.error(javax.servlet.http.HttpServletRequest)
2018-06-19 11:00:28.028  INFO 2708 --- [           main] s.w.s.m.m.a.RequestMappingHandlerMapping : Mapped "{[/error],produces=[text/html]}" onto public org.springframework.web.servlet.ModelAndView com.brtbeacon.commvc.web.controller.DefaultErrorController.errorHtml(javax.servlet.http.HttpServletRequest,javax.servlet.http.HttpServletResponse)
2018-06-19 11:00:28.059  INFO 2708 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/webjars/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2018-06-19 11:00:28.059  INFO 2708 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2018-06-19 11:00:28.081  INFO 2708 --- [           main] .m.m.a.ExceptionHandlerExceptionResolver : Detected ResponseBodyAdvice implementation in jsonpAdviceAdapter
2018-06-19 11:00:28.108  INFO 2708 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**/favicon.ico] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2018-06-19 11:00:28.151  WARN 2708 --- [           main] org.thymeleaf.templatemode.TemplateMode  : [THYMELEAF][main] Template Mode 'HTML5' is deprecated. Using Template Mode 'HTML' instead.
2018-06-19 11:00:28.672  INFO 2708 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Registering beans for JMX exposure on startup
2018-06-19 11:00:28.674  INFO 2708 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Bean with name 'druidDataSource' has been autodetected for JMX exposure
2018-06-19 11:00:28.680  INFO 2708 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Located MBean 'druidDataSource': registering with JMX server as MBean [com.alibaba.druid.pool:name=druidDataSource,type=DruidDataSource]
2018-06-19 11:00:28.742  INFO 2708 --- [           main] b.c.e.u.UndertowEmbeddedServletContainer : Undertow started on port(s) 8080 (http)
2018-06-19 11:00:28.749  INFO 2708 --- [           main] c.b.e.SpringBootTemplateApplication      : Started SpringBootTemplateApplication in 4.446 seconds (JVM running for 4.884)
2018-06-19 11:00:56.949  INFO 2708 --- [  XNIO-2 task-1] io.undertow.servlet                      : Initializing Spring FrameworkServlet 'dispatcherServlet'
2018-06-19 11:00:56.950  INFO 2708 --- [  XNIO-2 task-1] o.s.web.servlet.DispatcherServlet        : FrameworkServlet 'dispatcherServlet': initialization started
2018-06-19 11:00:56.965  INFO 2708 --- [  XNIO-2 task-1] o.s.web.servlet.DispatcherServlet        : FrameworkServlet 'dispatcherServlet': initialization completed in 13 ms
```